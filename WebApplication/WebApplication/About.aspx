﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="WebApplication.About" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
        <asp:Table runat="server" style="margin-top:5%;">
            <asp:TableRow>
                <asp:TableCell><label style="padding:5px;">Customer ID</label></asp:TableCell>
                <asp:TableCell><input runat="server" id="CustomerID" type="text" placeholder="ID" /></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell><label style="padding:5px;">Company Name</label></asp:TableCell>
                <asp:TableCell><input runat="server" id="CompanyName" type="text" placeholder="Company Name" required /></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell><label style="padding:5px;">Contact Name</label></asp:TableCell>
                <asp:TableCell><input runat="server" id="ContactName" type="text" placeholder="Contact Name" /></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell><label style="padding:5px;">Contact Title</label></asp:TableCell>
                <asp:TableCell><input runat="server" id="ContactTitle" type="text" placeholder="Contact Title" /></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell><label style="padding:5px;">Address</label></asp:TableCell>
                <asp:TableCell><input runat="server" id="Address" type="text" placeholder="Address" /></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell><label style="padding:5px;">City</label></asp:TableCell>
                <asp:TableCell><input runat="server" id="City" type="text" placeholder="City" /></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell><label style="padding:5px;">Region</label></asp:TableCell>
                <asp:TableCell><input runat="server" id="Region" type="text" placeholder="Region" /></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell><label style="padding:5px;">Postal Code</label></asp:TableCell>
                <asp:TableCell><input runat="server" id="PostalCode" type="text" placeholder="Postal Code" /></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell><label style="padding:5px;">Country</label></asp:TableCell>
                <asp:TableCell><input runat="server" id="Country" type="text" placeholder="Country" /></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell><label style="padding:5px;">Phone</label></asp:TableCell>
                <asp:TableCell><input runat="server" id="Phone" type="text" placeholder="Phone" /></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell><label style="padding:5px;">Fax</label></asp:TableCell>
                <asp:TableCell><input runat="server" id="Fax" type="text" placeholder="Fax" /></asp:TableCell>
            </asp:TableRow>
        </asp:Table> 
    <asp:Button ID="btnInsert" runat="server" onclick="btnInsert_Click" Text="Insert"/>
        
</asp:Content>
