﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Insert Data" style="margin-top:5%;"/>  
    <asp:GridView ID="CustTable" runat="server" 
        style="margin-top:1%;" EmptyDataText="No Records" CssClass="table table-hover table-striped" GridLines="None" AllowPaging="True" 
        OnPageIndexChanging="CustTable_PageIndexChanging" AutoGenerateColumns="false" OnRowCancelingEdit="CustTable_RowCancelingEdit" OnRowDeleting="CustTable_RowDeleting" OnRowEditing="CustTable_RowEditing" OnRowUpdating="CustTable_RowUpdating" AllowSorting="True" OnSorting="CustTable_Sorting" AllowCustomPaging="True">
        <Columns>
            <asp:TemplateField HeaderText="Actions" ItemStyle-Width="150px" ItemStyle-HorizontalAlign="Center" >
                <ItemTemplate>
                    <asp:Button ID="btn_Edit" runat="server" Text="Edit" CommandName="Edit" />
                    <asp:Button ID="btn_Delete" runat="server" Text="Delete" CommandName="Delete"
                        OnClientClick="return confirm('Delete data ?');"/>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:Button ID ="btn_Update" runat="server" Text="Update" CommandName="Update" 
                        OnClientClick="return confirm('Update data ?');"/>
                    <asp:Button ID ="btn_Cancel" runat="server" Text="Cancel" CommandName="Cancel" />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Customer ID" SortExpression="CustomerID">
                <ItemTemplate>
                    <asp:Label ID="lbl_CustomerID" runat="server" Text='<%#Eval("CustomerID") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Company Name" SortExpression="CompanyName">
                <ItemTemplate>
                    <asp:Label ID="lbl_CompanyName" runat="server" Text='<%#Eval("CompanyName") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="tb_CompanyName" runat="server" Text='<%#Eval("CompanyName") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Contact Name" SortExpression="ContactName">
                <ItemTemplate>
                    <asp:Label ID="lbl_ContactName" runat="server" Text='<%#Eval("ContactName") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="tb_ContactName" runat="server" Text='<%#Eval("ContactName") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Contact Title" SortExpression="ContactTitle">
                <ItemTemplate>
                    <asp:Label ID="lbl_ContactTitle" runat="server" Text='<%#Eval("ContactTitle") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="tb_ContactTitle" runat="server" Text='<%#Eval("ContactTitle") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Address" SortExpression="Address">
                <ItemTemplate>
                    <asp:Label ID="lbl_Address" runat="server" Text='<%#Eval("Address") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="tb_Address" runat="server" Text='<%#Eval("Address") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="City" SortExpression="City">
                <ItemTemplate>
                    <asp:Label ID="lbl_City" runat="server" Text='<%#Eval("City") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="tb_City" runat="server" Text='<%#Eval("City") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Region" SortExpression="Region">
                <ItemTemplate>
                    <asp:Label ID="lbl_Region" runat="server" Text='<%#Eval("Region") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="tb_Region" runat="server" Text='<%#Eval("Region") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Postal Code" SortExpression="PostalCode">
                <ItemTemplate>
                    <asp:Label ID="lbl_PostalCode" runat="server" Text='<%#Eval("PostalCode") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="tb_PostalCode" runat="server" Text='<%#Eval("PostalCode") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Country" SortExpression="Country">
                <ItemTemplate>
                    <asp:Label ID="lbl_Country" runat="server" Text='<%#Eval("Country") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="tb_Country" runat="server" Text='<%#Eval("Country") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Phone" SortExpression="Phone">
                <ItemTemplate>
                    <asp:Label ID="lbl_Phone" runat="server" Text='<%#Eval("Phone") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="tb_Phone" runat="server" Text='<%#Eval("Phone") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Fax" SortExpression="Fax">
                <ItemTemplate>
                    <asp:Label ID="lbl_Fax" runat="server" Text='<%#Eval("Fax") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="tb_Fax" runat="server" Text='<%#Eval("Fax") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerSettings Mode="Numeric" />
    </asp:GridView>
    
</asp:Content>

